import pytest
from django.forms.renderers import DjangoTemplates
from django.test.html import parse_html

from django_spim.widgets import ObjectImage


@pytest.mark.parametrize(
    "initial, data",
    (
        ("string", "string"),
        (123, 456),
    ),
)
def test_has_changed(initial, data):
    assert ObjectImage().has_changed(initial, data) is False


def test_format_value():
    widget = ObjectImage()
    assert widget.format_value(None) is None
    assert widget.format_value("") is None
    assert widget.format_value("español") == "español"
    assert widget.format_value(42.5) == "42.5"


def test_value_omitted_from_data():
    widget = ObjectImage()
    assert widget.value_omitted_from_data({}, {}, "field") is True
    assert widget.value_omitted_from_data({"field": "value"}, {}, "field") is False


@pytest.mark.parametrize("name, value, attrs, expected", (("name", "value", {}, "html"),))
def test_render(name, value, attrs, expected):
    widget = ObjectImage()
    output = widget.render(name, value, attrs=attrs, renderer=DjangoTemplates())
    assert parse_html(output)
    # TODO: assert parse_html(output) == parse_html(expected)
    # see: django.test.testcases import SimpleTestCase, assert_and_parse_html
