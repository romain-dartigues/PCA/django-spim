from tempfile import gettempdir

import pytest
from django.conf import settings


def pytest_configure():
    settings.configure(
        DATABASES={
            "default": {
                "ENGINE": "django.db.backends.sqlite3",
                "NAME": ":memory:",
            }
        },
        INSTALLED_APPS=[
            "django.contrib.auth",
            "django.contrib.contenttypes",
            "django_spim",
        ],
        SIM_MEDIA_ROOT=gettempdir(),
        TEMPLATES=[
            {
                "BACKEND": "django.template.backends.django.DjangoTemplates",
                "APP_DIRS": True,
            },
        ],
    )


@pytest.fixture(autouse=True, scope="session")
def configure_media_root(tmp_path_factory):
    """Change SIM_MEDIA_ROOT to a temporary directory"""
    settings.SIM_MEDIA_ROOT = tmp_path_factory.getbasetemp()
