from base64 import b64decode

image = dict(
    GIF=b64decode("R0lGODlhAQABAAAAACH5BAEAAAAALAAAAAABAAEAAAIA"),
    JPEG=b64decode(
        "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAP/"
        "////////////////////////////////////////////////////////////////////////////////////"
        "/wgALCAABAAEBAREA/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxA="
    ),
    PNG=b64decode("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQAAAAA3bvkkAAAACklEQVR4nGNgAAAAAgABSK+kcQAAAABJRU5ErkJggg=="),
    SVG=b'<svg xmlns="http://www.w3.org/2000/svg" width="1" height="1"/>',
    WEBP=b64decode("UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA=="),
)
"""minimal images"""
