# stdlib
from pathlib import Path

# pytest
import pytest

# django
from django.core.exceptions import SuspiciousFileOperation
from django.db.utils import IntegrityError

# SPIM
from django_spim import models, settings
from django_spim.models import Image, ObjectBased


@pytest.mark.parametrize(
    "filename, exception",
    (
        (settings.SIM_MEDIA_ROOT, IntegrityError),
        ("/", SuspiciousFileOperation),
        (f"{settings.SIM_MEDIA_ROOT!s}/../", SuspiciousFileOperation),
        (0, TypeError),
        ("", IntegrityError),
        (".", IntegrityError),
        ("..", SuspiciousFileOperation),
    ),
)
def test_image_invalid_name(filename, exception):
    with pytest.raises(exception):
        Image(filename)


@pytest.mark.parametrize(
    "filename, basename, dirname, thumbname",
    (
        ("image.jpg", "image.jpg", "", ".thumbnails/image.jpg.jpg"),
        ("im.age.jpg", "im.age.jpg", "", ".thumbnails/im.age.jpg.jpg"),
        ("some/subdir/to/image.png", "image.png", "some/subdir/to", "some/subdir/to/.thumbnails/image.png.jpg"),
    ),
)
def test_image_name(filename, basename, dirname, thumbname):
    image = Image(filename)
    assert image.is_pinky is False
    assert image.is_thumb is False
    assert str(image) == filename
    assert image.basename == basename
    assert image.dirname == dirname
    assert image.thumbname == thumbname


@pytest.mark.parametrize(
    "filename, media_url, url",
    (
        ("img.jpg", "/pictures", "/pictures/img.jpg"),
        ("img.jpg", "", "/img.jpg"),
        ("some/subdir/to/image.png", "", "/some/subdir/to/image.png"),
        ("some/subdir/to/image.png", "/pictures", "/pictures/some/subdir/to/image.png"),
    ),
)
def test_image_url(filename, media_url, url):
    settings.SIM_MEDIA_URL = models.SIM_MEDIA_URL = media_url
    image = Image(filename)
    assert image.url() == url


def test_image_1():
    name = "minimal.gif"
    image = Image(name)
    assert str(image) == name


# With real data ############################################################
# IMAGE_FILES = list(Path(__file__).parent.absolute().joinpath("images").glob("*.*"))


def test_image_with_pinkies(monkeypatch, tmp_path):
    image_sample = Path(__file__).parent.absolute().joinpath("images", "dew.png")
    assert image_sample.exists()
    monkeypatch.setitem(Image.optimize.__globals__, "SIM_JPEGTRAN", None)
    monkeypatch.setitem(Image.optimize.__globals__, "SIM_PNGOUT", None)
    image_file = tmp_path.joinpath(image_sample.name)
    image_file.absolute().parent.mkdir(parents=True, exist_ok=True)
    assert image_file.write_bytes(image_sample.read_bytes())

    image = Image(image_file)
    thumbnail = image.thumbnail.save()
    assert thumbnail.is_thumb is True
    assert thumbnail.original == image
    assert thumbnail.thumbnail == thumbnail
    assert thumbnail.thumbnail != image

    pinky1 = image.pinky((66, 99)).save()
    pinky2 = image.pinky((128, 192)).save()
    assert pinky1.is_pinky is True
    assert pinky1.is_thumb is False
    assert pinky1.original == image
    assert pinky1.thumbnail == thumbnail
    assert pinky1.url().endswith("/test_image_with_pinkies0/.thumbnails/dew.png-66x99.jpg")

    assert image.shrink_size(w=4096, h=4096) == image.original.size
    assert image.shrink_size(w=160, h=160) == (106, 160)
    assert image.shrink_size(w=160, h=480) == (160, 240)

    assert list(image.pinkies) == [pinky1, pinky2]

    # TODO: image.nail
    # image.nail((380, 380))
    # image.thumbnail.url() == "/foo"
    # assert nail.is_pinky is False
    # assert nail.is_thumb is True
    # assert nail.original == image
    #
    # assert nail.url() == "/foo"

    assert image.delete() is None


# Container #################################################################
# ObjectBased ###############################################################

import django.contrib.auth.models


@pytest.mark.django_db
def test_container_init():
    admin = django.contrib.auth.models.User(username="admin")
    with pytest.raises(AssertionError):
        ObjectBased(admin)
    admin.save()
    object_based_image = ObjectBased(admin)
    assert object_based_image.dirname == f"{admin._meta.model_name}_{admin.pk}"
