from io import BytesIO
from tempfile import NamedTemporaryFile

import pytest
from django.core.files.uploadedfile import InMemoryUploadedFile

from django_spim.utils import check_nail, is_image, scale, which

from . import image


@pytest.mark.parametrize(
    "w, h, x, y, maximum, expected",
    (
        (1024, 1024, 1024, 1024, True, (1024, 1024)),
        (1024, 1024, 480, 320, True, (320, 320)),
    ),
)
def test_scale(w, h, x, y, maximum, expected):
    assert scale(w, h, x, y, maximum) == expected


@pytest.mark.parametrize(
    "data, expected",
    (
        (image["GIF"], "gif"),
        (image["JPEG"], "jpeg"),
        (image["PNG"], "png"),
        (image["SVG"], None),  #: not a known image
        (image["WEBP"], False),  #: image, but not allowed format
    ),
)
def test_is_image(data: bytes, expected):
    # test it as an UploadedFile
    uploaded_file = InMemoryUploadedFile(BytesIO(data), None, None, None, None, None)
    assert is_image(uploaded_file) == expected
    # same test as a file on the filesystem
    with NamedTemporaryFile(buffering=0) as temp_file:
        temp_file.write(data)
        temp_file.flush()
        assert is_image(temp_file.name) == expected
    assert is_image(temp_file.name) is None, "file does not exist"


def test_which_finds_executable(monkeypatch, tmp_path):
    name = 'something'
    temp_file = tmp_path.joinpath(name)
    monkeypatch.setitem(which.__globals__, 'PATH', [tmp_path.as_posix()])
    with pytest.raises(OSError):
        which(name)
    temp_file.touch()
    with pytest.raises(OSError):
        which(name)
    temp_file.chmod(0o755)
    assert which(name) == temp_file.as_posix()
