from django.forms.utils import flatatt
from django.forms.widgets import Widget
from django.template.loader import render_to_string


class ObjectImage(Widget):
    @staticmethod
    def has_changed(initial, data) -> False:  # pylint: disable=unused-argument
        # Always return False to avoid form validation
        # Would it be better if the form 'data' could be the same as 'initial'?
        return False

    def render(self, name, value, attrs=None, renderer=None):
        return render_to_string(
            "django_spim/inc.container_thumbs.html",
            {"attrs": flatatt(self.build_attrs(attrs)), "container": value},
        )
