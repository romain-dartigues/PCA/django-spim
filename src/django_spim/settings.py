# stdlib
from os import PathLike
from os.path import realpath
from pathlib import Path

# django
from django.conf import settings

SIM_MEDIA_ROOT: str | PathLike = realpath(
    getattr(settings, "SIM_MEDIA_ROOT", f"{settings.MEDIA_ROOT.rstrip('/')}/pictures")
)
"""SIM_MEDIA_ROOT: local path where the pictures will be saved.

This path *must* exists with read/write access to the program beforehand.
This module will not touch data outside this directory.

See also :var:`django.conf.settings.MEDIA_ROOT`."""

SIM_MEDIA_URL: str = realpath(getattr(settings, "SIM_MEDIA_URL", f"{settings.MEDIA_URL.rstrip('/')}/pictures"))
"""SIM_MEDIA_URL: URL handling the media served from :var:`SIM_MEDIA_ROOT`.
See also :var:`django.conf.settings.MEDIA_URL`."""

SIM_THUMBNAILS: str = getattr(settings, "SIM_THUMBNAILS", ".thumbnails")
"SIM_THUMBNAILS: thumbnails directory"

SIM_THUMB_SIZE: tuple[int, int] = getattr(settings, "SIM_THUMB_SIZE", (128, 128))
"SIM_THUMB_SIZE: maximum thumbnails dimensions"

SIM_PINKIES: str = getattr(settings, "SIM_PINKIES", SIM_THUMBNAILS)
"""SIM_PINKIES: pinkies directory
See also :var:`SIM_THUMBNAILS`."""

SIM_PINKY_SIZE: tuple[int, int] = getattr(settings, "SIM_PINKY_SIZE", (16, 16))
"""SIM_PINKY_SIZE: minimum pinkies dimensions"""

SIM_THUMB_EXT: str = getattr(settings, "SIM_THUMB_EXT", "jpg")
"""SIM_THUMB_EXT: extension and file format used for the thumbnails (ie: jpg, png, gif).
Note: JPEG are the smallest in most cases."""

SIM_PINKY_EXT: str = getattr(settings, "SIM_PINKY_EXT", SIM_THUMB_EXT)
"""SIM_PINKY_EXT: extension and file format used for the pinkies.
See also :var:`SIM_THUMB_EXT`."""

SIM_BACKEND: str = getattr(settings, "SIM_BACKEND", "PIL")
"""SIM_BACKEND: backend used for image processing (ie: PIL, VIPS, IM).
Note: for my usage, PIL was the fastest; see :file:`backend_README.txt`."""

SIM_JPEGTRAN = getattr(settings, "SIM_JPEGTRAN", "/usr/bin/jpegtran")
"""SIM_JPEGTRAN: full path to the jpegtran binary
"""

SIM_PNGOUT = getattr(settings, "SIM_PNGOUT", "/usr/bin/pngout")
"""SIM_PNGOUT: full path to the pngout binary
"""
