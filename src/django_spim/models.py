# stdlib
from __future__ import annotations

import os
import os.path
import re
import subprocess
import weakref
from os import PathLike, stat_result
from pathlib import Path
from shutil import rmtree
from typing import Optional
from warnings import warn

# django
from django.apps import apps
from django.core.exceptions import (
    ImproperlyConfigured,
    ObjectDoesNotExist,
    SuspiciousOperation,
)
from django.db.models.base import Model
from django.db.utils import IntegrityError
from django.urls import reverse
from django.utils._os import safe_join

from .settings import (
    SIM_BACKEND,
    SIM_JPEGTRAN,
    SIM_MEDIA_ROOT,
    SIM_MEDIA_URL,
    SIM_PINKIES,
    SIM_PINKY_EXT,
    SIM_PNGOUT,
    SIM_THUMB_EXT,
    SIM_THUMB_SIZE,
    SIM_THUMBNAILS,
)

# local
from .utils import is_image

try:
    backend = __import__(f"{__package__}.backend_{SIM_BACKEND}", fromlist=[""])
except ImportError as import_error:  # pragma: no cover
    raise ImproperlyConfigured from import_error

try:
    # ensure the configuration path is resolved
    SIM_MEDIA_ROOT = Path(SIM_MEDIA_ROOT).resolve(True)
except FileNotFoundError as error:
    raise ImproperlyConfigured from error


class _ImageMixin:
    path: Path  #: the file as a :class:`pathlib.Path`

    def __eq__(self, other) -> bool:
        return (
            type(other) is type(self)
            and self.path == other.path
            and (not self.path.exists() or self.path.samefile(other.path))
        )

    def __repr__(self) -> str:  # pragma: no cover
        return f"<{self.__class__.__name__}: {self.path}>"


class Image(_ImageMixin):
    """An Image object

    A *thumbnail* is a reduced version of the original image,
    while a *pinky* is one, of possibly many, alternate (reduced) size of the original image.
    """

    __pinky = re.compile(r"(?P<name>.+)-(?P<size>(?P<w>\d+)x(?P<h>\d+))\.(?P<ext>\w+)$")
    __original: Optional["Image"] = None
    __size: Optional[tuple[int, int]] = None
    __stat: Optional[stat_result] = None

    is_pinky: Optional[bool] = None
    is_thumb: Optional[bool] = None

    def __init__(self, filename: str | PathLike, original: Optional["Image"] = None):
        self.path = Path(safe_join(SIM_MEDIA_ROOT, filename)).resolve()
        if self.path == SIM_MEDIA_ROOT:
            raise IntegrityError("unnamed file")
        dirname = os.path.basename(self.dirname)
        self.is_pinky = dirname == SIM_PINKIES and bool(self.__pinky.match(self.basename))
        self.is_thumb = not self.is_pinky and dirname == SIM_THUMBNAILS
        self.__original = original
        assert original is None or (self.is_pinky or self.is_thumb)

    @property
    def basename(self) -> str:
        """file base name (ie: file.jpg)"""
        return self.path.name

    @property
    def dirname(self) -> str:
        """file relative directory without any leading slash or dot

        Example::

            >>> Image("image.jpg").dirname
            ''
            >>> Image("path/to/image.jpg").dirname
            'path/to'
        """
        dirname = str(self.path.relative_to(SIM_MEDIA_ROOT).parent)
        return "" if dirname == "." else dirname

    @property
    def pinkies(self):
        """Iter over existing pinkies"""
        directory = self.path.parent.joinpath(SIM_PINKIES)
        basename = self.original.path.name
        try:
            for item in directory.iterdir():
                match = self.__pinky.match(item.name)
                if match and match.group("name") == basename:
                    yield Image(item, original=self)
        except OSError:
            return

    @property
    def thumbnail(self) -> "Image":
        """Returns the Image object of the current object thumbnail"""
        if self.is_thumb:
            return self
        if self.is_pinky:
            return self.original.thumbnail
        return self.__class__(self.thumbname, original=self)

    @property
    def thumbname(self) -> str:
        """Return the original image's thumbnail name"""
        return os.path.join(self.original.dirname, SIM_THUMBNAILS, f"{self.original.basename}.{SIM_THUMB_EXT}")

    def shrink_size(self, w=0, h=0) -> tuple[int, int]:
        """Shrink larger image size while keeping the aspect ratio"""
        assert isinstance(w, int) and isinstance(h, int)
        assert w or h and 0 <= w and 0 <= h
        width, height = self.original.size
        if w > width or h > height:
            # shrink only
            return width, height
        if width > w:
            width, height = w, max(height * w // width, 1)
        if height > h:
            width, height = max(width * h // height, 1), h
        return width, height

    def pinkyname(self, size: tuple[int, int]) -> str:
        """Return a pinky name for the original image with `size` dimensions

        See :var:`self.__pinky`
        """
        size = self.shrink_size(*size)
        name = os.path.join(
            self.original.dirname,
            SIM_PINKIES,
            f"{self.original.basename}-{size[0]}x{size[1]}.{SIM_PINKY_EXT}",
        )
        return name.lstrip(os.path.sep)

    def pinky(self, size: tuple[int, int]) -> "Image":
        return self.__class__(self.pinkyname(size), original=self)

    @property
    def stat(self) -> stat_result:
        if not self.__stat:
            try:
                self.__stat = self.path.stat()
            except OSError:
                return os.stat_result((0,) * 10)
        return self.__stat

    @property
    def size(self) -> tuple[int, int]:
        """return the size as a tuple (width, height) or None"""
        if not self.__size:
            self.__size = backend.size(self.path)
        return self.__size

    @property
    def original(self) -> "Image":
        # XXX: get rid of this function or make it smarter
        if not self.__original:
            if self.is_pinky or self.is_thumb:
                raise ObjectDoesNotExist
            self.__original = self
        return self.__original

    def __str__(self) -> str:
        """Path relative to :data:`settings.SIM_MEDIA_ROOT`"""
        return f"{self.path.relative_to(SIM_MEDIA_ROOT)}"

    def __hash__(self) -> int:
        return hash(self.path)

    def nail(self, size: tuple[int, int]) -> None:
        """generate the thumbnail (or pinky)"""
        destination = self.pinky(size) if self.is_pinky else self
        # TODO: if original size <= size and unix: symlink (require changes in __pinky)
        backend.nail(src=self.original.path, dst=destination.path, size=self.shrink_size(*size))

    nail.alters_data = True

    def optimize(self) -> bool | None:
        """Reduce the size of the picture while keeping it's quality

        - drop the JPEG EXIF
        - use an optimized deflate algorithm for PNG

        Returns True on success, False on failure, None if no
        optimization is available.
        """
        image_format = is_image(self.path)
        if image_format == "jpeg" and SIM_JPEGTRAN:
            temp_file = self.path.parent.joinpath("jpeg.tmp")  # TODO: use a NamedTemporaryFile here
            status = subprocess.call(
                (
                    SIM_JPEGTRAN,
                    # 				'-copy','none',
                    # 				'-progressive',
                    "-optimize",
                    "-outfile",
                    temp_file.as_posix(),
                    self.path.as_posix(),
                )
            )
            if status == 0:
                temp_file.rename(self.path)
                return True
            try:
                temp_file.unlink()
            except OSError:
                pass
            return False
        if image_format == "png" and SIM_PNGOUT:
            status = subprocess.call(
                (
                    SIM_PNGOUT,
                    "-q",
                    self.path.as_posix(),
                )
            )
            return not status
        return None

    optimize.alters_data = True

    def save(self) -> "Image":
        assert self.basename, "unnamed file can not be created"
        # TODO: try: except XXX, err: raise IntegrityError(err)
        if self.is_thumb:
            self.nail(SIM_THUMB_SIZE)
        elif self.is_pinky:
            size = self.size
            if not size:
                size = self.__pinky.search(self.basename).group("w", "h")
                size = int(size[0]), int(size[1])
            self.nail(size)
        else:
            raise NotImplementedError
        self.optimize()
        return self

    save.alters_data = True

    def delete(self) -> None:
        assert self.basename, "unnamed file can not be deleted"
        if self.is_pinky:
            return self.path.unlink(True)
        if self.is_thumb:
            return  # cannot remove just a thumbnail
        # if is original, remove file + thumbnail + pinkies
        for path in self.pinkies:
            path.path.unlink()
        if self.thumbnail.path.exists():
            self.thumbnail.path.unlink()
        self.path.unlink(True)
        self.__size = None
        return

    delete.alters_data = True

    def url(self) -> str:
        return f"/{os.path.join(SIM_MEDIA_URL, self.dirname, self.basename).removeprefix('/')}"

    def get_absolute_url(self) -> str:
        return reverse("image", kwargs={"path": self.url()})
        # return reverse("xxx:image", kwargs={"NSS": self.nss})


class Container(_ImageMixin):
    """Simulate an object based container for use with a database object.

    :param path: path to a directory
    """

    filter_image = True
    # TODO: order by

    def __init__(self, path: str | os.PathLike | None = None):
        self.path = Path(safe_join(SIM_MEDIA_ROOT, path)).resolve()
        if self.path == SIM_MEDIA_ROOT:
            raise IntegrityError("unnamed path")

    @property
    def dirname(self) -> str:
        """relative directory path without leading slash or dot

        Example::

            >>> Container("path/to_container").dirname
            'path/to_container'
            >>> Container("path/to/to_container/image.jpg").dirname
            'path/to_container'
        """
        return str(self.path.relative_to(SIM_MEDIA_ROOT))

    def is_container(self, path: Optional[str | PathLike] = None, simulate=False) -> bool:
        """Check whenever is a container

        :param path: path to check if set, otherwise test the current object
        :param simulate: if True, also check if the path is an existing directory on the filesystem
        """
        path = self.path if path is None else Path(path)
        return path.name not in {SIM_THUMBNAILS, SIM_PINKIES} and (simulate or path.is_dir())

    def __iter__(self):
        """Iter through a container, yielding other :class:`Container` or :class:`Image`"""
        try:
            for path in self.path.iterdir():
                if self.is_container(path):
                    yield self.__class__(path=path)
                elif not self.filter_image or is_image(path):
                    yield Image(path)
        except OSError:
            pass

    def __str__(self):
        return str(self.dirname)

    def __hash__(self):
        return hash(self.path)

    def __bool__(self):
        """Return True if the container exist on the filesystem"""
        warn(f"do not check truth on {self!r}", DeprecationWarning)
        return self.path.is_dir()

    def save(self):
        """Create directory and missing parents"""
        if self.path.is_dir():
            return self
        if self.is_container(simulate=True):
            try:
                self.path.mkdir(0o755, True, True)
                return self
            except OSError as err:
                raise IntegrityError from err
        else:
            raise IntegrityError(f"does not validate: {self!s}")

    save.alters_data = True

    def delete(self):
        if self.path == SIM_MEDIA_ROOT or not self.is_container(simulate=True):
            raise SuspiciousOperation(f"attempting to delete: {self.path!s}")
        rmtree(self.path)

    delete.alters_data = True

    def __getitem__(self, y: Image | PathLike) -> Container | Image:
        if isinstance(y, Image):
            for x in self:
                if x == y:
                    return x
            raise KeyError(y)
        try:
            path = safe_join(self.path, y)
        except ValueError as err:
            raise SuspiciousOperation(f"{err!s}: {y!r}") from err
        for x in self:
            if x.path == path:
                return x
        raise KeyError(y)


class ObjectBased(Container):
    """Simulate an object based container for use with a database object.

    Example::

        >>> from django.contrib.auth.models import User
        >>> user = User.objects.first()
        >>> object_based_image = ObjectBased(user)
        >>> object_based_image.model.is_superuser  # model = the user
        True
        >>> object_based_image.dirname
        '/user_1'
    """

    __format = re.compile(r"/(?P<name>[^/]+)_(?P<id>\d+)/?$")
    model_pk = None
    model_class = None
    model_name = None
    _model = None

    def __init__(self, obj: Model):
        assert obj.pk, f'"{obj!r}" needs to have a value for field "id" before this relationship can be used'
        self._model = weakref.proxy(obj)
        self.model_class = type(obj)
        self.model_pk = obj.pk
        self.model_name = ".".join((obj.__module__.replace(".models", ""), obj.__class__.__name__))
        super().__init__(path=f"{obj._meta.model_name!s}_{obj.pk}")

    @staticmethod
    def from_model(model: str, *args, **kwargs) -> ObjectBased | None:
        """Return a container from a given model

        Usage::

           ObjectBased.from_model('example.article', pk=12)
        """
        m = apps.get_model(*model.rsplit(".", 1))
        if m:
            return ObjectBased(m.objects.get(*args, **kwargs))
        return None

    def is_container(self, path: Optional[str, PathLike] = None, simulate=False) -> bool:
        return super().is_container(path, simulate) and bool(self.__format.search(path))

    @property
    def model(self) -> Model:
        """L'instance du model dont cet objet est le container"""
        if isinstance(self._model, self.model_class):
            return self._model
        return self.model_class.objects.get(pk=self.model_pk)


# TODO: make a DateBased(Container)
