# stdlib
import re
import subprocess
from ctypes import CDLL
from ctypes.util import find_library

# django
from django.core.exceptions import ImproperlyConfigured

# local
from .utils import check_nail, which

_im = CDLL(find_library("MagickWand"))


if not (which("identify") and which("convert")):
    raise ImproperlyConfigured("image magick not found")


if _im._name:  # pylint: disable=protected-access
    # ctype size is approximately 4* faster
    def size(filename):
        wand = _im.NewMagickWand()
        if not _im.MagickReadImage(wand, filename):
            _im.DestroyMagickWand(wand)
            return None
        size = _im.MagickGetImageWidth(wand), _im.MagickGetImageHeight(wand)
        _im.DestroyMagickWand(wand)
        return size

else:
    __size = re.compile(r"\s(\d+)x(\d+)\s")

    def size(filename):
        # identify filename
        p = subprocess.Popen(
            ("identify", "--", filename), stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True
        )
        if p.wait() == 0:
            size = __size.search(p.stdout.read()).groups()
            return int(size[0]), int(size[1])
        return None


def nail(src, dst, size):
    check_nail(src, dst, size)
    p = subprocess.Popen(("convert", "--", "%s[%dx%d]" % (src, size[0], size[1]), dst), close_fds=True)
    if p.wait() != 0:
        raise OSError
