# stdlib
import os

# dependencies
import vipsCC.VImage

# local
from .utils import check_nail


def size(filename):
    try:
        im = vipsCC.VImage.VImage(filename)
        return im.Xsize(), im.Ysize()
    except vipsCC.VError:
        return None


def nail(src, dst, size):
    # see: http://www.vips.ecs.soton.ac.uk/supported/current/doc/html/libvips/VipsFormat.html
    # set JPEG quality 80%: dst += ':80'
    # set PNG compression 9: dst += ':9'
    check_nail(src, dst, size)
    img = vipsCC.VImage.VImage(src)
    fac = img.Xsize() / float(size[0]), img.Ysize() / float(size[1])
    img.shrink(*fac).write(("%s:90" if dst.rsplit(os.extsep)[-1].lower() in ("jpg", "jpeg") else "%s") % dst)
    # 	img.affine(fac[0], 0, 0, fac[1], 0, 0, 0, 0, size[0], size[1]).write(dst)
    os.chmod(dst, 0o644)
