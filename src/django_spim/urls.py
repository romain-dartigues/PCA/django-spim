from django.conf import settings
from django.conf.urls.static import static

app_name = "django_spim"

urlpatterns = static(settings.SIM_MEDIA_URL, document_root=settings.SIM_MEDIA_ROOT)
