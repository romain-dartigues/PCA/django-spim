# stdlib
import os.path

# django
from django.apps import apps
from django.contrib.admin import ModelAdmin, site
from django.contrib.admin.views import main
from django.contrib.messages import info
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template.context_processors import csrf
from django.template.loader import render_to_string
from django.urls import re_path, reverse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_str
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_protect

# dependencies
from django_pca_utils import normalize_filename

# local
from .models import Image as ImageModel
from .models import ObjectBased
from .utils import is_image

csrf_protect_m = method_decorator(csrf_protect)


# test on images
class FakeMeta:
    abstract = not True
    app_label = __name__.split(".")[-2]
    model_name = None
    object_name = None
    swapped = False
    verbose_name = None
    verbose_name_plural = None
    get_ordered_objects = list

    def __init__(self, name, verbose_name=None, verbose_plural=None):
        self.app_config = apps.get_app_config("django_spim")
        self.model_name = name
        self.verbose_name = verbose_name or name
        self.verbose_name_plural = verbose_plural or f"{self.verbose_name}s"


class Image:
    _meta = FakeMeta("image")


class AdminImage(ModelAdmin):
    """
    See :meth:`django.contrib.admin.options.ModelAdmin.get_urls`
    """

    def changelist_view(self, request, extra_context=None):
        # url: image/$
        if not self.has_change_permission(request, None):
            raise PermissionDenied
        main.ChangeList(request)  # FIXME: should it be ModelAdmin.get_changelist(request)

    def add_view(self, request, form_url="", extra_context=None):
        # url: image/add/$
        pass

    def delete_view(self, request, object_id, extra_context=None):
        # url: image/(.+)/delete/$
        # TODO: see contrib.admin.actions.delete_selected()
        pass

    def change_view(self, request, object_id, form_url="", extra_context=None):
        # url: image/(.+)/$
        pass

    def get_urls(self):
        return [
            re_path(
                r"^(?P<name>[a-zA-Z0-9]+\.[a-zA-Z0-9]+)_(?P<pk>\d+)/$",
                self.view_images_upload,
                name="django_spim_objectbased",
            ),
        ] + super().get_urls()

    @csrf_protect_m
    def view_images_upload(self, request, name, pk):  # pylint: disable=invalid-name
        dictionary = csrf(request)
        try:
            container = ObjectBased.from_model(name, pk=pk)
        except ObjectDoesNotExist:
            raise Http404()
        # 		if not request.user.is_superuser \
        # 		and not models.article.credits.filter(role=1, user=request.user): # FIXME
        # 			return HttpResponseForbidden()
        if request.method == "POST":
            del_list = request.POST.getlist("del")
            for item in del_list:
                try:
                    container[item].delete()
                    info(
                        request,
                        _('The %(name)s "%(obj)s" was deleted successfully.')
                        % {"name": "Image", "obj": force_str(item)},
                    )
                except KeyError:
                    pass  # TODO: message
                except BaseException:
                    pass  # TODO: message
            add_list = request.FILES.getlist("add")
            if add_list and not container:
                container.save()
            for src in add_list:
                if not is_image(src):
                    continue  # TODO: add error message
                item = normalize_filename(src.name)
                path = container.fullpath + os.path.sep + item
                if os.path.exists(path):
                    continue  # TODO: error message
                with open(path, "wb") as dst:
                    for chunk in src.chunks():
                        dst.write(chunk)
                image = ImageModel(dst.name)
                image.optimize()
                image.thumbnail.save()

            if "_save" in request.POST:
                destination = reverse("admin:%s_change" % name.replace(".", "_").lower(), args=(pk,))
            else:
                destination = reverse("admin:django_spim_objectbased", args=(name, pk))

            return HttpResponseRedirect(destination)
        # end
        dictionary["request"] = request
        dictionary["container"] = container
        return HttpResponse(render_to_string("django_spim/adm.container.html", dictionary))


site.register([Image], AdminImage)
