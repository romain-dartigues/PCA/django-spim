# stdlib
import os

# dependencies
from PIL.Image import Resampling
from PIL.Image import open as open_image

# local
from .utils import check_nail


def RESAMPLE(w: int, h: int):
    """Return a resampling option depending on the image size

    Quality (low to high): NEAREST, BILINEAR, BICUBIC, ANTIALIAS
    """
    size = w * h
    if size < 8192:
        return Resampling.NEAREST
    if size < 16384:
        return Resampling.BILINEAR
    return Resampling.BICUBIC


def size(filename):
    try:
        return open_image(filename).size
    except IOError:
        return None


def nail(src, dst, size: tuple[int, int]):
    """
    .. Note::
       This function will systematically save a `dst` in RGB even when the
       `src` was in a different colorspace.
    """
    check_nail(src, dst, size)
    img = open_image(src)
    # 	assert img.mode in ('L', 'RGB'), 'invalid mode %s for %s' %(img.mode, src)
    if img.mode not in ("L", "RGB"):
        img = img.convert("RGB")
    img.thumbnail(size, RESAMPLE(*size))
    img.save(dst, optimize=True)
    os.chmod(dst, 0o644)
