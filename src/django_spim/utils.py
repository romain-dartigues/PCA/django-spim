# stdlib
import imghdr
import os
from os import PathLike

# django
from django.core.files.uploadedfile import UploadedFile

# local
from . import settings

PATH = os.environ.get("PATH", "").split(":")


def check_nail(src: PathLike | str, dst: PathLike | str, size: tuple[int, int]) -> None:
    assert src != dst
    assert isinstance(size, tuple) and isinstance(size[0], int) and isinstance(size[1], int)
    if size[0] <= settings.SIM_PINKY_SIZE[0] or size[1] <= settings.SIM_PINKY_SIZE[1]:
        raise ValueError(f"side must be > 16: {size}")
    path = os.path.dirname(dst)
    if not os.path.isdir(path):
        os.makedirs(path, 0o755)


def which(name: str | PathLike) -> str | PathLike:
    """return the full path of the first executable `name` in PATH or None

    See :var:`os.environ['PATH']`

    :param name: name of the executable
    :raises OSError: when `name` is not found
    """
    for p in PATH:
        path = p + os.path.sep + name
        # 		path = os.path.join(p, name)
        if os.access(path, os.X_OK) and os.path.isfile(path):
            return path
    raise OSError(f"file not found: {name}")


def scale(w: int, h: int, x: int, y: int, maximum=True) -> tuple[int, int]:
    """Return the nearest possible to ``x/y`` size while keeping the ``w/h`` ratio
    - `w`, `h`: initial width/height
    - `x`, `y`: limits
    - `maximum`: at most; sides are scaled to fit
      if False,  at least; sides won't be smaller
    """
    nw = y * w // h
    nh = x * h // w
    if maximum ^ (nw >= x):
        return nw or 1, y
    return x, nh or 1


def is_image(path: str | PathLike) -> str | bool | None:
    """returns the extension of the image

    Returns:

    - the image extension if is an allowed image
    - False, if is not an allowed image
    - None, if unable to read file or not an image
    """
    assert isinstance(path, (str, PathLike, UploadedFile))
    what = None
    if isinstance(path, UploadedFile):
        peek = path.read(32)
        path.seek(0)
        what = imghdr.what(None, peek)
    else:
        try:
            what = imghdr.what(path)
        except IOError:
            pass
    if what in {"gif", "png", "jpeg"}:
        return what
    return False if what else None
