
Images

- les images sont associees a un article
- upload
- generation des thumbnails *si besoin est*
- admin: les liens sont d'une des deux formes suivante:

   - ``^(?P<model>[^/]+)/(?P<pk>\d+)/image/`` puis les actions

      - `article/2/image/upload/`  add new image for article 2
      - `article/2/image/`         list article's images
      - `article/2/image/name.jpg` view image and thumbnails

   - ``^image/(?:(?P<model>[^/]+)/(?P<pk>\d+)/)?`` puis les actions

      - `image/`                 list *tree*
      - `image/upload/`          date based upload
      - `image/2009/`            list *tree* for 2009
      - `image/2009/03/`         list images for March 2009
      - `image/2009/03/name.jpg` view image and thumbnails


images tree
===========

::
   .  # images root (could be MEDIA_URL)
   │
   ├─ {obj_name}/{obj_id}/
   │   ├─ .thumbnails/
   │   │   ├─ {image_name}-{custom_size}.png
   │   │   └─ {image_name}.png
   │   └─ {image_name}
   │
   └─ {year}/{month}/
       ├─ .thumbnails/
       │   ├─ {image_name}-{custom_size}.png
       │   └─ {image_name}.png
       └─ {image_name}


##############################################################################

Base:
- parcours un repertoire et en liste les images et les repertoires::

   >>> list(Base('.'))
   [Image('xyz.jpg'), Directory('foo')]


DateBased:
- requiert des repertoires sous la forme::

   YYYY/MM/images*


ArticleBased:
- arborescence sous la forme::

   prefix?/object_name-object_id/images*

##############################################################################

.. :: vim:ft=rst ts=3 sw=3 ai et
