
TODO: convert to RGB:
   PIL::
      if image.mode not in ('L', 'RGB'): image = image.convert('RGB')


>>> from django_spim import backend_PIL, backend_IM, backend_VIPS
(454, 337)
>>> backend_IM.size('dst.jpg')
(128, 95)
>>> backend_IM.nail('src.jpg', 'foo.jpg', (128,95))


Performances
============

On my computer and for the planned usage (JPEG between 10KiB and 2MiB) `PIL`
is the fastest; on a 200K JPEG::

   import timeit

   data = []
   for B in ('PIL', 'VIPS', 'IM'):
    data.append((
     B,
     timeit.timeit('size("src.jpg")', 'from django_spim.backend_%s import size'%B, number=100)/100,
     timeit.timeit('nail("src.jpg", "dst.jpg", (128,95))', 'from django_spim.backend_%s import nail'%B, number=100)/100
    ))

   # processing time per image
   [('PIL',  0.0013225507736206055, 0.046537919044494627),
    ('VIPS', 0.0030386710166931154, 0.050931670665740968),
    ('IM',   0.026680948734283446,  0.17311400175094604)]

.. :: vim:set ft=rst ts=3 sw=3:
