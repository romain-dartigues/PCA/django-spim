# README

SPIM: Simple Pictures/Images Manager for Django.

Because I didn't find elegant to query a database for files stored on a **local**
filesystem, which is, itself, a database.

## Licence

This project is licensed under the terms of the BSD 3-clause "New" or "Revised" license.
